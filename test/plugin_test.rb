# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: 2016 Harald Sitter <sitter@kde.org>

require_relative 'test_helper'

# Dud base class. We mocha this for functionality later.
class Plugin
  class Config
    class ArrayValue
      @@kdebugsactivity_defaulted = false

      def self.kdebugsactivity_defaulted
        # At least one ArrayValue must have had a default value with the bugs
        # activity channel. This is technically a very specific value but
        # asserting any had it should be good enough.
        @@kdebugsactivity_defaulted
      end

      def initialize(_, params = {})
        return unless params[:default] == %w[#kde-bugs-activity]

        @@kdebugsactivity_defaulted = true
      end
    end

    def self.register(_)
    end
  end

  def map(*args)
  end

  def bot
  end
end

require 'bugzilla'

class PluginTest < Test::Unit::TestCase

  # NB: mocha is stupid with the quotes and can't tell single from double!

  def setup
    ENV.delete('XDG_DATA_DIRS')
    ENV.delete('XDG_CONFIG_HOME')

    assert(Plugin::Config::ArrayValue.kdebugsactivity_defaulted,
      'blacklist ArrayValue should have had the bugs activity channel blacklisted')

    config = mock('config')
    # Do not give an api_token as we need the environment to take over.
    config.stubs(:[]).with('phabricator.api_token').returns(nil)
    # This default value is also set in the rb and asserted via our duds
    config.stubs(:[]).with('bugzilla.blacklist').returns(%w[#kde-bugs-activity])
    config.stubs(:[]).with('bugzilla.url_blacklist').returns([])
    bot = mock('bot')
    bot.stubs(:config).returns(config)
    Plugin.any_instance.stubs(:bot).returns(bot)

    @config = config
  end

  def teardown
  end

  def message_double
    channel = mock('message-channel')
    channel.stubs(:name).returns('#message-double-channel')
    mock('message').tap { |m| m.stubs(:channel).returns(channel) }
  end

  def test_get_unreplied
    message = message_double
    message.stubs(:message).returns('yolo brooom bug 123')

    plugin = BugzillaPlugin.new
    plugin.expects(:bug).with(message, number: '123')
    plugin.unreplied(message)
  end

  def test_get_unreplied_url_blacklist
    @config.stubs(:[]).with('bugzilla.url_blacklist').returns(%w[#message-double-channel])

    message = message_double
    message.stubs(:message).returns('yolo brooom bug https://bugs.kde.org/show_bug.cgi?id=123')

    plugin = BugzillaPlugin.new
    plugin.expects(:bug).never
    plugin.unreplied(message)
  end

  def test_bug
    message = message_double
    message.expects(:reply).with('KDE bug 359887 in neon (general) "Neon packages change DISTRIB_ID in /etc/lsb-release" [normal,FIXED] https://bugs.kde.org/show_bug.cgi?id=359887')

    VCR.use_cassette(__method__) do
      plugin = BugzillaPlugin.new
      plugin.bug(message, { :number => 359887 })
    end
  end

  def test_bug_fail
    message = message_double
    message.expects(:notify).with('Bug not found (ノಠ益ಠ)ノ彡┻━┻ JSONError {:message=>"Bug #1 does not exist.", :code=>101}')

    VCR.use_cassette(__method__) do
      plugin = BugzillaPlugin.new
      plugin.bug(message, { :number => 1 })
    end
  end

  def test_get_unreplied_multi_match
    message = message_double
    message.stubs(:message).returns('yolo brooom bug 123 and bug 321')

    plugin = BugzillaPlugin.new
    plugin.expects(:bug).with(message, number: '123')
    plugin.expects(:bug).with(message, number: '321')
    plugin.unreplied(message)
  end

  def test_get_unreplied_multi_match_url
    message = message_double
    message.stubs(:message).returns('yolo brooom https://bugs.kde.org/show_bug.cgi?id=366701 and https://bugs.kde.org/show_bug.cgi?id=366702')

    plugin = BugzillaPlugin.new
    plugin.expects(:bug).with(message, number: '366701')
    plugin.expects(:bug).with(message, number: '366702')
    plugin.unreplied(message)
  end

  def test_skip
    message = message_double
    message.channel.stubs(:name).returns('#kde-bugs-activity')
    plugin = BugzillaPlugin.new
    plugin.unreplied(message)
  end
end
