# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2016 Harald Sitter <sitter@kde.org>

require 'faraday'
require 'json'
require 'uri'
require 'logger'

require_relative 'representation'

module Bugzilla
  # Connection logic wrapper.
  class Connection
    attr_accessor :uri

    # FinerStruct wrapper around jsonrpc returned hash.
    class JSONRepsonse < FinerStruct::Immutable
      def initialize(response)
        super(JSON.parse(response.body, symbolize_names: true))
      end
    end

    def initialize
      @uri = URI.parse('https://bugs.kde.org')
    end

    def call(meth, **kwords)
      response = get(meth, **kwords)
      raise "HTTPError #{response.status}" if response.status != 200
      response = JSONRepsonse.new(response)
      raise "JSONError #{response.error}" if response.error
      response.result
    end

    private

    def get(meth, **kwords)
      client.get do |req|
        req.url '/jsonrpc.cgi'
        req.headers['Content-Type'] = 'application/json'
        req.params = {
          method: meth,
          params: [kwords].to_json
        }
      end
    end

    def client
      @client ||= Faraday.new(@uri.to_s) do |faraday|
        faraday.request :url_encoded
        # faraday.response :logger, ::Logger.new(STDOUT), bodies: true
        faraday.adapter Faraday.default_adapter
      end
    end
  end
end
