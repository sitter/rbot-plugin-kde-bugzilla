# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2016 Harald Sitter <sitter@kde.org>

require_relative 'connection'
require_relative 'representation'

module Bugzilla
  # A Bug!
  class Bug < Representation
    def web_url
      "#{connection.uri}/show_bug.cgi?id=#{id}"
    end

    class << self
      def get(id, connection = Connection.new)
        new(connection, connection.call('Bug.get', ids: [id])[:bugs][0])
      end
    end
  end
end
